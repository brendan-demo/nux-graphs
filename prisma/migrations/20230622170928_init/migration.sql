/*
  Warnings:

  - You are about to drop the column `meta` on the `logging` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "logging" DROP COLUMN "meta";
