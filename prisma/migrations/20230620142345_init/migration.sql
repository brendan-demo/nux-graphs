/*
  Warnings:

  - A unique constraint covering the columns `[hour]` on the table `nuclei_by_minute` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "nuclei_by_minute_hour_key" ON "nuclei_by_minute"("hour");
