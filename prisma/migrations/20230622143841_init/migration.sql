-- CreateTable
CREATE TABLE "nuclei_by_hour" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "hour" TIMESTAMP(3) NOT NULL,
    "hourstr" TEXT NOT NULL,
    "sumOfScans" INTEGER NOT NULL,

    CONSTRAINT "nuclei_by_hour_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "nuclei_by_hour_hour_key" ON "nuclei_by_hour"("hour");
