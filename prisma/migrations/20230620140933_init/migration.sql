-- CreateTable
CREATE TABLE "nuclei_by_minute" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "hour" TIMESTAMP(3) NOT NULL,
    "hourstr" TEXT NOT NULL,
    "sumOfScans" INTEGER NOT NULL,

    CONSTRAINT "nuclei_by_minute_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "audit" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "rowcount" INTEGER NOT NULL,
    "runningTime" INTEGER NOT NULL,
    "fullresponse" JSONB NOT NULL,

    CONSTRAINT "audit_pkey" PRIMARY KEY ("id")
);
