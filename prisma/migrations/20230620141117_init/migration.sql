/*
  Warnings:

  - Added the required column `updatedAt` to the `nuclei_by_minute` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "nuclei_by_minute" ADD COLUMN     "updatedAt" TIMESTAMP(3) NOT NULL;
