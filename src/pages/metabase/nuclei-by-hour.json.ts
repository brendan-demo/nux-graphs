import type { APIRoute } from "astro";
import { nucleiScans } from "../../db/getdata";

export const get: APIRoute = async ({ params, request }) => {
    const data = await nucleiScans().catch(e => console.error(e))
    return {
        body: JSON.stringify(data)
    }
}