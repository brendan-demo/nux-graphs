import type { APIRoute } from "astro";
import axios from "axios";
import { nucleiScans } from "../../db/loaddata";

const metabaseHeaders = {
  headers: {
    "Content-Type": "application/json",
    "X-Metabase-Session": import.meta.env.METABASE_PASSWORD,
  },
};

async function runIt() {
  const sourceURL = "https://pdtm.metabaseapp.com/api/card/30/query";
  const cardQueryParams = {
    ignore_cache: false,
    collection_preview: false,
    parameters: [],
  };

  const response = await axios.post(
    sourceURL,
    cardQueryParams,
    metabaseHeaders
  );

  await nucleiScans(JSON.stringify(response.data));
}

export const get: APIRoute = async ({ params, request }) => {
  await runIt();
  return {
    body: JSON.stringify({ message: "Ok" }),
  };
};

export const post: APIRoute = async ({ params, request }) => {
  await runIt();
  return {
    body: JSON.stringify({ message: "Ok" }),
  };
};
