import type { APIRoute } from "astro";
import { nucleiScans } from "../db/getdata";

export const get: APIRoute = async ({params, request}) => {
    const data = await nucleiScans()
    const arr = data.map(e => e.sumOfScans)
    return {
        body: JSON.stringify(arr)
    }
}