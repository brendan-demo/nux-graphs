import type { APIRoute } from "astro";

function randomArr(len: number) {
    const MAX = 65535;
    const MIN = 4086;    
    const arr = Array.from({ length: len }, () => {
        return Math.floor(Math.random() * (MAX - MIN) + MIN)
    });
    return arr;
}

function fakeArr(n: number) {
    const sequence = [0, 1];
    for (let i = 2; i < n; i++) {
        let nextNum = sequence[i - 1] + sequence[i - 2];
        sequence.push(nextNum)
    }

    return sequence;
}

function sineCurveSequence(amplitude: number, frequency: number, count: number) {
    let sequence = [];
  
    // Generate the sequence along a sine curve up to the given count
    for (let i = 0; i < count; i++) {
      let angle = (i / count) * 2 * Math.PI;
      let nextNum = amplitude * Math.sin(frequency * angle);
      sequence.push(nextNum);
    }
  
    return sequence;
  }

export const get: APIRoute = ({params, request}) => {
    //const arr = randomArr(65)
    const arr = sineCurveSequence(100, 1, 65)
    return {
        body: JSON.stringify(arr)
    }
}