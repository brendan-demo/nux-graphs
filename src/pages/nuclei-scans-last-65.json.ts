import type { APIRoute } from "astro";
import { nucleiScans } from "../db/getdata";

export const get: APIRoute = async ({params, request}) => {
    const data = await nucleiScans()
    const arr = data.map(e => e.sumOfScans)
    const last65 = arr.slice(0, 65);
    return {
        body: JSON.stringify(last65.reverse())
    }
}