import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

async function runDataLoad(data: string) {
    const json = JSON.parse(data);
    
    json?.data?.rows.forEach(async (r: any[]) => {
        let [hourS, tool, sum] = r
        const thisHour = new Date(hourS);
        await prisma.nuclei_by_hour.upsert({
            where: {
                hour: thisHour
            },
            update: {
                sumOfScans: sum
            },
            create: {
                hour: thisHour,
                hourstr: hourS,
                sumOfScans: sum
            }
        })
    });
    
    await prisma.audit.create({
        data: {
           rowcount: json?.row_count,
           runningTime: json?.running_time,
           fullresponse: json
        }
    })
}

export async function nucleiScans(data: string) {
    return runDataLoad(data)
    .then(async () => {
        await prisma.$disconnect()
    }).catch(async (e) => {
        console.error(e);
        await prisma.$disconnect()
    })
}