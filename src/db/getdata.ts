import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export async function nucleiScans() {
    const allscans = await prisma.nuclei_by_hour.findMany({
        orderBy: {
            hour: "desc"
        }
    });
    return allscans;
}