# Nux Graphs

## Production

https://nux-graphs.vercel.app/

## Stack

- Astro.build
- Database: Supabase
- Scheduled Tasks: Upstash
- Kafka logging: Upstash